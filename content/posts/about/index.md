---
title: "About"
date: 2018-12-07T07:52:26+09:00
draft: false
authors: ["antonio"]
---

{{< figure src="./reinvent2018.jpg" >}}

# Why

My company sent me to the AWS re:invent 2018 in Las Vegas so I created this small site to put together all the notes I initially wrote as Markdown files along with extra info, slides, photos and videos so I can easily share it with coworkers and friends when they ask about it and also go through it when I need it.

# Contents

Most of the content is for the sessions I participated during the conference. 

Everything is tagged with the technologies/topics discussed, the level of the session (introductory, advanced & expert)

All the posts are tagged with the technologies and topics discussed.

*Tip*: start with the recommended ones

# Useful resources

- [AWS re:invent official site](https://reinvent.awsevents.com/)
- [Event Catalog](https://www.portal.reinvent.awsevents.com/connect/search.ww)
- [re:invent videos](https://reinventvideos.com/) All the videos of all re:invent conferences.
- [All the slides](https://www.slideshare.net/AmazonWebServices)

# Random thoughts

At work...

serverless: we are using serverless components (lambda, dynamo, etc) in the right way
ssm parameter store: recommended tool to store configs and credentials

topics I want to dive on

- orchestration: CloudFormation / CDK / CodePipeline
- system management
- Security automation
- architectural patterns: serverless step functions, 


# Anecdotes

- I made 20 seconds in the DeepRacer challenge at the event. The winner was from my company too and made in 14 secs! More about [DeepRacer](https://aws.amazon.com/deepracer/) and [how it was during the event (Youtube)](https://www.youtube.com/watch?v=jD0Z9D9Y0Sc)

- I talked with some AWS people about orchestration and code pipeline, commit, build and how to use them to orchestrate. There's people even in AWS doesn't like CloudFormation, that's why they created [AWS CDK](https://awslabs.github.io/aws-cdk/)

