---
title: "CTD410 - Analyze Amazon CloudFront and Lambda@Edge Logs to Improve Customer Experience"
date: 2018-11-28T15:15:00-08:00
draft: false
tags: ["type:workshop", "recommended", "level:expert", "quicksight", "athena"]
authors: ["antonio"]
---

# Overview

- Recommended workshop with materials available in github [here](https://github.com/aws-samples/amazon-cloudfront-log-analysis/)
- Build an end-to-end serverless solution to analyze Amazon CloudFront logs using AWS Glue and Amazon Athena, generate visualization to derive deeper insights using Amazon QuickSight

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/cPM73BdlbtkvR3" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/analyze-amazon-cloudfront-and-lambdaedge-logs-to-improve-customer-experience-ctd410-aws-reinvent-2018" title="Analyze Amazon CloudFront and Lambda@Edge Logs to Improve Customer Experience (CTD410) - AWS re:Invent 2018" target="_blank">Analyze Amazon CloudFront and Lambda@Edge Logs to Improve Customer Experience (CTD410) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Material

https://github.com/aws-samples/amazon-cloudfront-log-analysis/

# Original description
Nowadays, web servers are often fronted by a global content delivery network, such as Amazon CloudFront, to accelerate delivery of websites, APIs, media content, and other web assets. In this hands-on-workshop, learn to improve website availability, optimize content based on devices, browser and user demographics, identify and analyze CDN usage patterns, and perform end-to-end debugging by correlating logs from various points in a request-response pipeline. Build an end-to-end serverless solution to analyze Amazon CloudFront logs using AWS Glue and Amazon Athena, generate visualization to derive deeper insights using Amazon QuickSight, and correlate with other logs such as CloudWatch logs to provide finer debugging experiences. Discuss how you can extend the pipeline you just built to generate deeper insights needed to improve the overall experience for your users.