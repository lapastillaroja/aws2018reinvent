---
title: "DVC07 - AWS CloudFormation Techniques from the Dutch Trenches"
date: 2018-11-28T14:00:00-08:00
draft: false
tags: ["type:dev chat"]

---

# Overview

- Not very interesting...

# Original description
AWS CloudFormation is by far the best provisioning tool for AWS architectures. It’s also complex, and it took us months to master of all the features and fix the lacking parts. In this talk, we share best practices and open source tools and techniques, for example, custom resources for missing AWS resources, managing secrets, advanced configuring of AWS managed services, easy templating, and secure deployment tooling. This talk is full of demonstrations and real experiences from modern DevOps enterprises, traditional companies, and born-in-the-cloud startups. Get inspired, and share your ideas and techniques the same way we do.