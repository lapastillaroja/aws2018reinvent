---
title: "DAT205-R - [REPEAT] Databases on AWS: The Right Tool for the Right Job"
date: 2018-11-28T13:45:00-08:00
draft: false
tags: ["databases", "dynamodb", "rds", "time series", "elastic cache", "level:introductory", "recommended"]
authors: ["antonio"]
---

# Overview

- Totally recommended!
- Watch it when doubting what data storage type to use
- Didn't join during the conference but watched it later. 

## Slides

No slides at this moment

## Youtube

{{< youtube -pb-DkD6cWg >}}

# Original description
Shawn Bice, VP of Non-Relational Databases at AWS, discusses a purpose-built strategy for databases, where you choose the right tool for the job. Shawn explains why your application should drive the requirements of a database, not the other way around. We introduce AWS databases that are purpose-built for your application use cases. Learn why you should select different database services to solve different aspects of an application, and watch a demonstration in which application use cases lend themselves well to specific data services. If you're a developer building modern applications that require high performance, scale, and functional databases, and you're trying to determine which relational and non-relational data services to use, this session is for you. Please join us for a speaker meet-and-greet following this session at the Speaker Lounge (ARIA East, Level 1, Willow Lounge). The meet-and-greet starts 15 minutes after the session and runs for half an hour.