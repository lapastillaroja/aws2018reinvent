---
title: "SRV401-R1 - Optimizing Your Serverless Applications"
date: 2018-11-27T10:00:00-08:00
draft: false
tags: ["serverless", "level:expert", "recommended"]
authors: ["antonio"]
---

# Overview

- **Totally recommended** for anyone using serverless

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/LqQdZB9wtkJTWH" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/optimizing-your-serverless-applications-srv401r2-aws-reinvent-2018" title="Optimizing Your Serverless Applications (SRV401-R2) - AWS re:Invent 2018" target="_blank">Optimizing Your Serverless Applications (SRV401-R2) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube

{{< youtube sSSMTSn2xmA >}}

# Memo

## start

note: check new service released `AWS Firecracker`

Places where you can impact perf.
- your function
- execution environment

## your function

components: handler, event, context ;)

note: one event at a time, no multi event processing

### pre-handler

note: use naming `pre-handler`

^ **initialize only what you really need**
^ get secrets and configs

### AWS SSM parameter store

(we are already using it!)
- support hierarchies
- plain text or encrypted
- can send not of changes to SNS/Lambda
- can be secured
- cloudtrail
- can be tagged
- available via SDK

### best practices

- separate handler from core logic
- use functions to `transform`, not ~~transform~~
- dynamic logic via config
  - per function
  - cross func
^ read only what you need. ie
  - properly indexed db
  - query filter in Aurora
  - use s3 select

#### don't do!
don't filter db data at lambda level

### no orchestration in code!

don't sleep, retry, etc

use `AWS step functions`
- has decission tree 

### project & repository scoping

`monorepo` == anti-pattern for FaaS

1. if functions share an event source they can go in the same repo, if not they go in their own repo as separate applications
  - simplifies permissions
2. if func share an event source but require varying different imported packages, make them their own function files/jars, etc
  - keep dep bloat minimized per function

note: don't do routing inside the lambda function

**note antonio: I don't agree on this!!**

### recap

see photo

## execution environment

aws optimization: download code, create environment
your optimization: partial cold start (bootstrap the runtime), warm start (start your code)


### X-Ray
lambda & api gw support xray (need to be enabled)
trace example: lambda service -> lambda function (cold start!)

### tweak your function's computer 

see photos (twweak & cost)

### multhrithreading? maybe!

< 1.8 GB is still single core
  CPU bound workloads won't see gains
> 1.8 GB is multicore
  CPU bound workloads will gains, but need to multithread

**IO workloads will like see gains**

### lambda execution modles
- sync (api gw)
- async (sns, s3)
- poll based (dynamodb, kinesis, changes)

### Lambda API

use lambda api to start an async execution
- built-in SQA 

### microservices
see photo (iceberg)

### gateways and routers

note: don't use gateway if not necessary
see photo

### concureency across models
see photo

lambda per function concurrency controls
important: useful to control to not to overload downstream services

#### scaling/concurrency contorls
see photo

### persistency
see photo

### retry/failure handling
see photo

### lambda dead letter queues
see photo
note: use it (for async use-case)

### networking

invocations can oly come in via the lambda API
see photo
note: lambda are note created within our vpc. instead, aws created an ENI and connect to the aws managed vpc

note: **putting lambda func within a VPC doesn't provide any extra security**

do I need a VPC? (see photos)

#### basic vpc design

don't put lambda in shared vpc with other services
it can block lambda creation

best (see photo)
- always configure a minimum of 2 AZs

### security

lambda permissions model
- execution policies
- function policies
  - used for sync and async invocations. eg: actions on bucket X can invoke function Z

### SAM
policy templates: cool! useful to save time creating policies
see photo and use 

### recap
see photo

## questions
ENI

# Photos

{{< figure src="./IMG_20181127_130153.jpg" >}}
{{< figure src="./IMG_20181127_130154.jpg" >}}
{{< figure src="./IMG_20181127_130205.jpg" >}}
{{< figure src="./IMG_20181127_130515.jpg" >}}
{{< figure src="./IMG_20181127_130906.jpg" >}}
{{< figure src="./IMG_20181127_131441.jpg" >}}
{{< figure src="./IMG_20181127_131817.jpg" >}}
{{< figure src="./IMG_20181127_132108.jpg" >}}
{{< figure src="./IMG_20181127_132309.jpg" >}}
{{< figure src="./IMG_20181127_132647.jpg" >}}
{{< figure src="./IMG_20181127_132705.jpg" >}}
{{< figure src="./IMG_20181127_133208.jpg" >}}
{{< figure src="./IMG_20181127_133304.jpg" >}}
{{< figure src="./IMG_20181127_133548.jpg" >}}
{{< figure src="./IMG_20181127_133751.jpg" >}}
{{< figure src="./IMG_20181127_133835.jpg" >}}
{{< figure src="./IMG_20181127_133955.jpg" >}}
{{< figure src="./IMG_20181127_134149.jpg" >}}
{{< figure src="./IMG_20181127_134344.jpg" >}}
{{< figure src="./IMG_20181127_134557.jpg" >}}
{{< figure src="./IMG_20181127_134624.jpg" >}}
{{< figure src="./IMG_20181127_134715.jpg" >}}
{{< figure src="./IMG_20181127_134846.jpg" >}}
{{< figure src="./IMG_20181127_134954.jpg" >}}
{{< figure src="./IMG_20181127_135152.jpg" >}}
{{< figure src="./IMG_20181127_135156.jpg" >}}
{{< figure src="./IMG_20181127_135410.jpg" >}}
{{< figure src="./IMG_20181127_135527.jpg" >}}
{{< figure src="./IMG_20181127_135614.jpg" >}}
{{< figure src="./IMG_20181127_135817.jpg" >}}
{{< figure src="./IMG_20181127_135906.jpg" >}}

# Original description

Are you an experienced serverless developer? Do you want a handy guide for unleashing the full power of serverless architectures for your production workloads? Are you wondering whether to choose a stream or an API as your event source, or whether to have one function or many? In this session, we discuss architectural best practices, optimizations, and handy cheat codes that you can use to build secure, high-scale, high-performance serverless applications. We use real customer scenarios to illustrate the benefits.
