---
title: "ARC309-R1 - From One to Many: Evolving VPC Design"
date: 2018-11-27T13:00:00-08:00
draft: false
tags: ["vpc", "networking", "level:advanced", "recommended"]
authors: ["antonio"]
---

# Overview

- Super super dense session about VPCs but recommended if you are into it
- Explain how VPCs, subnes, VPC Gateway and more
- also explains why the new `AWS Transit Gateway` service exits and ho can help you to manage the routing between VPCs


## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/EmE3hG4u0OediL" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/from-one-to-many-evolving-vpc-design-arc309r1-aws-reinvent-2018" title="From One to Many: Evolving VPC Design (ARC309-R1) - AWS re:Invent 2018" target="_blank">From One to Many: Evolving VPC Design (ARC309-R1) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube

{{< youtube 8K7GZFff_V0 >}}

# Memo

## VPC ip space design

tip: recommend to start with CIDR /16
it's possible to attach multiple CIDR blocks to the same VPC
but it has caveats,

## subnet

note: a subnet, a container for routing tables!
isolation: provided by firewalls

## VPC subnet design

note: subnets lives in an AZ

three categories (/16)

IPv4

- public subnet (/22): it's routable, has internet access
- private subnet (/20): no internet, only local route or access on pre
- VPN only subnet (20): don't have EIP attached. You can associate a

IPv6

- public
- ... (need to check video/slides)


note: every subnet can communicate with each other
NACL!!


# AWS Transit Gateway

per region

can be

# Photos

{{< figure src="./IMG_20181127_165729.jpg" >}}
{{< figure src="./IMG_20181127_165734.jpg" >}}
{{< figure src="./IMG_20181127_165947.jpg" >}}

# Original description

As more customers adopt Amazon VPC architectures, the features and flexibility of the service are encountering the obstacles of evolving design requirements. In this session, we follow the evolution of a single regional VPC to a multi-VPC, multi-region design with diverse connectivity into on-premises systems and infrastructure. Along the way, we investigate creative customer solutions for scaling and securing outbound VPC traffic, securing private access to Amazon S3, managing multi-tenant VPCs, integrating existing customer networks through AWS Direct Connect, and building a full VPC mesh network across global regions. Please join us for a speaker meet-and-greet following this session at the Speaker Lounge (ARIA East, Level 1, Willow Lounge). The meet-and-greet starts 15 minutes after the session and runs for half an hour.
