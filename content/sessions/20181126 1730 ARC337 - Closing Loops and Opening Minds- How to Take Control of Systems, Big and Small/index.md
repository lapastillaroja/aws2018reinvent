---
title: "ARC337 - Closing Loops and Opening Minds: How to Take Control of Systems, Big and Small"
date: 2018-11-26T17:30:00-08:00
draft: false
tags: ["control planes", "level:advanced"]
authors: ["antonio"]
---

# Overview

- Very niche topic: Control Planes! Still seems a important topic if you want to understand the theory *necessary* to build a stable system.
- It was difficult for me to follow, a *new* topic for me. Examples were just a few

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/f9bzsbwuudAKy0" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/closing-loops-and-opening-minds-how-to-take-control-of-systems-big-and-small-arc337-aws-reinvent-2018" title="Closing Loops and Opening Minds: How to Take Control of Systems, Big and Small (ARC337) - AWS re:Invent 2018" target="_blank">Closing Loops and Opening Minds: How to Take Control of Systems, Big and Small (ARC337) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube

{{< youtube O8xLxNje30M >}}

# Memo

## overview
Point: how we build simple & stable control systems?

what goes into high quality designs? (see photo)

## start

how we make **trade offs** in design
1. security
2. durability
3. availability
4. speed


control planes vs data planes
cp: are often a bigger design challenge than the data planes that they support

poorly designed control planes have the ability to cause large outages, or worse: misconfiguration & ...

what do control planes do in the Cloud?
- manage the lifecycle for resources
- (merged): provision SW + service configuration (ie: enable/disable features)
- provision user configuration

## is control theory useful?

control theory 101 (see photo)

- exists as a science!
- lots of materials

example: autoscaling is a (dynamic) control system

components: 

- input
- algorithms 
- actuator (cohete)

-- doing it in a loop (close system)
note: has to be fast!

note: recommended book *designing distributed control systems* (wiley)

## 10 patterns for Controlling the Cloud

### p1: checksum all of the things

ie: s3 availability event july 20, 2008

yaml file are sensible to truncation

### p2: cryptographic auth everywhere

- encrypt and authen. everything! CP are powerful and sec. critical systems
- be able to revoke and rotate every credentials (also certificates)
- prevent humans access to prod credentials
- never allow a non-prod control plane to talk the prod data plane

### p3: cells, shells, and poison tasters

- we divide up our control planes horiz. into regions, av. zones and cells (panel in us-esat-1 doesn't affect us-east-2)

compartmentalize CP so that the data plane is insulated from CP crashes

- poison tasters: 

### pt: async coupling

- sync system are very strongly coupled
- a prob in a sync. downstream dep has inmediate impact on the ups. callers
- one more

vs

- async coupling syst. tend to be more tolerant
- can make partial progress even 

### p5: closed feedback loops
see above graph

### p6: small pushes and large pulls

- very FAQ: is it better to push, or to pull?
  ie: should data planes....
  ... no lo pille!

### p7: avoiding cold starts and cold caches

- caches are bi-modal systems. super fast when they have entries, and slow when they are empty
- a thundering herd hitting a cold cache can prevent it from ever getting warm
- retry storms often need to be moteraed by throttles

### p8: throttles

T and rate-limits ore often needed to moderate problem requestors and to dampen fluctuating systems
IE: ELB and EC2

- takes careful work to ensure that throttling does ot imact the end customer exp.

### p3: deltas

what happens when we do have too much configuration state to push around?
- -> more efficient to compute deltas and distribute patches
- -> how we do that?
  - -> have versions!

### p10: modality and constant-work

- So far, we can build a losely coupled control plane, with deltas to minimize work, and throttles to keep things safe
- buuuuuut, what if a LOT of things change at the same time?
- We don't want to build up backlogs and queues and introduce lag

note: relational DB are terrible for CP (control planes)!!!

(What AWS does?)

- how dumb would it be to make a really really simple control plane?
- User calls an API that edits a config. file on S3. Push that configuration file every 10 secs!

Our NW health checks, including R53 Health checks are a good example

- happening all of the time
- results being published to consumers, all of the time
- zone or region failure = no difference!

note: to have a system checking (pulling) configs from S3 continously would be *only* $12 a year! it worths it! 100 nodes would be $1200 (?)

### whate did we learn about buildind stable systems?

(see photo)
- closing loops is critical, measure the progress!
- loose async coupling helps
- think about the modalities of the system
- our lessons are baked into amazon API GW and Lambda

See?: https://en.wikipedia.org/wiki/PID_controller

## QA session

- how to institualize through the company?
  - weekly sessions about several topics like this one
  - design reviews for each system
  - operational readiness review

# Photos

{{< figure src="./IMG_20181126_173343.jpg" >}}
{{< figure src="./IMG_20181126_173601.jpg" >}}
{{< figure src="./IMG_20181126_174158.jpg" >}}

# Original description

Whether it’s distributing configurations and customer settings, launching instances, or responding to surges in load, having a great control plane is key to the success of any system or service. Come hear about the techniques we use to build stable and scalable control planes at Amazon. We dive deep into the designs that power the most reliable systems at AWS. We share hard-earned operational lessons and explain academic control theory in easy-to-apply patterns and principles that are immediately useful in your own designs.
