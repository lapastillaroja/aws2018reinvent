---
title: "CON366 - [NEW LAUNCH!] Introducing AWS Cloud Map"
date: 2018-11-30T10:00:00-08:00
draft: false
tags: ["cloud map", "level:advanced"]
authors: ["antonio"]
---

# Overview

- New service: AWS CloudMap.
- Similar to [Consul](https://www.consul.io/) but AWS Managed

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/bFgzKW5vGPuo2a" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/new-launch-introducing-aws-cloud-map-con366-aws-reinvent-2018" title="[NEW LAUNCH!] Introducing AWS Cloud Map (CON366) - AWS re:Invent 2018" target="_blank">[NEW LAUNCH!] Introducing AWS Cloud Map (CON366) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube

{{< youtube fMGd9IUaotE >}}

# Original description

In this session we introduce AWS Cloud Map, a new service that lets you build the map of your cloud. It allows you to define friendly names for any resource such as S3 buckets, DynamoDB tables, SQS queues, or custom cloud services built on EC2, ECS, EKS, or Lambda. Your applications can then discover resource location, credentials, and metadata by friendly name using the AWS SDK and authenticated API queries. You can further filter resources discovered by custom attributes, such as deployment stage or version. AWS Cloud Map is a highly available service with rapid configuration change propagation.