---
title: "DEV302 - Monitor All Your Things: Amazon CloudWatch in Action with BBC "
date: 2018-11-28T12:15:00-08:00
draft: false
tags: ["monitoring", "level:advanced"]
authors: ["antonio"]
---

# Overview

- Worth to watch only for the demo of the new `CloudWatch Automatic Dashboards` (starts in minute 40). Until then was quite boring
- `CloudWatch Automatic Dashboards` are cool! Remember to tag all your resources!

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/60kHeHCpFzJca2" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/monitor-all-your-things-amazon-cloudwatch-in-action-with-bbc-dev302-aws-reinvent-2018" title="Monitor All Your Things: Amazon CloudWatch in Action with BBC (DEV302) - AWS re:Invent 2018" target="_blank">Monitor All Your Things: Amazon CloudWatch in Action with BBC (DEV302) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube

{{< youtube uuBuc6OAcVY >}}

# Memo


cloudwatch automatic dashboard -> use resource groups to group around services and provide useful insights only about it
tip: tag the resources!!!!

tip: use cloudwatch logs insights

# Original description

Come learn what's new with Amazon CloudWatch, and watch as we leverage new capabilities to better monitor our systems and resources. We also walk you through the journey that BBC took in monitoring its custom off-cloud infrastructure alongside its AWS cloud resources.