---
title: "SRV409-R1 - A Serverless Journey: AWS Lambda Under the Hood"
date: 2018-11-29T13:45:00-08:00
draft: false
tags: ["serverless", "firecracker", "level:expert", "recommended"]
authors: ["antonio"]
---

# Overview

- Totally recommended for people interested in details on how serverless works

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/bjDi1KtI3lEe1X" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/a-serverless-journey-aws-lambda-under-the-hood-srv409r1-aws-reinvent-2018" title="A Serverless Journey: AWS Lambda Under the Hood (SRV409-R1) - AWS re:Invent 2018" target="_blank">A Serverless Journey: AWS Lambda Under the Hood (SRV409-R1) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube

{{< youtube QdzV04T_kec >}}

# Memo

## overview
control planes and data planes (see photo)

(see photos)

- load balancing
- counting service: latency added less than 1.5 ms
- worker manager:
- worker: provision a secure environment for customer code execution (downloads code, - manage runtimes, notifies work manager)
- placement service: intelligence where 

## workers

### isolation

isolation and SW stack (see photo)
note: a sandbox is not shared between different functions

note: worker = separate ec2 instance!! (see photo nitro platform)
note: firecracker to spin thousands of micro VMs ?

virtio protocol

# Photos

TODO

# Original Description

Serverless computing allows you to build and run applications and services without thinking about servers. Serverless applications don't require you to provision, scale, and manage any servers. However, under the hood, there is a sophisticated architecture that takes care of all the undifferentiated heavy lifting for the developer. Join Holly Mesrobian, Director of Engineering, and Marc Brooker, Senior Principal of Engineering, to learn how AWS architected one of the fastest-growing AWS services. In this session, we show you how Lambda takes care of everything required to run and scale your code with high availability
