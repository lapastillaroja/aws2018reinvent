---
title: "DEV327-R Beyond the Basics- Advanced Infrastructure as Code Programming on AWS"
date: 2018-11-26T11:30:00-08:00
draft: false
tags: ["architecture", "infrastructure as code", "level:advanced"]
authors: ["antonio"]
---

# Overview

- Below expectations.
- Shows the potentials of Cloudformation macros, etc but doesn't go deep.
- It was boring, bad rhythm
- Useful link: AWS CDK (cloud dev kit) with Typescript examples: https://github.com/awslabs/aws-cdk/tree/master/examples/cdk-examples-typescript

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/EXgWAwWOncIbdZ" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/infrastructure-is-code-with-the-aws-cloud-development-kit-dev372-aws-reinvent-2018" title="Infrastructure Is Code with the AWS Cloud Development Kit (DEV372) - AWS re:Invent 2018" target="_blank">Infrastructure Is Code with the AWS Cloud Development Kit (DEV372) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube

{{< youtube EJVNuR2GRBc >}}

**Slides not available yet**


# Memo

## Declarative programming

describe **what we want**

options

- declarative
  - basic yaml/json
  - basic transforms (include/ SAM)
  - advanced transforms (macros, others: jinja/mustache)
- imperative
    - cdk (typescript)
    - ruby, ...

## deep dive: macros

- write short-hand, abbreviated instructions that expand automatically when deployed
- add utility functions, fe, iteration loops, strings, etc
- ensure resources are defined to comply to your standards
- **easy to share and reuse across stacks**
- **key benefit**: once macros are deployed, 

**macros are lambda functions!**

### Macro: execute Python
see photo

### Macro: global variables

this one interesting and easy to understand
see photos (global and code)

### Macro: additional resources

usefull to define a baseline
ex: add ACLs, add bucket policy, generate additional resources, override user, setting up defaults (defined in a side file)

see photos (global and code)

## Imperative programming in AWS CF

Options

- **AWS Cloud Dev Kit (developer preview)**
- Python
    - troposphere
- Other
    - **JS/TS**: Cloudformation
- **all of these still generate YAML/JSON code**

### AWS CDK

**looks awesome!!**
CDK CLI (compiler)

note: look for .gif demo somewhere and post here

## warpup

see photo

# All photos

{{< figure src="./IMG_20181126_113430.jpg" >}}
{{< figure src="./IMG_20181126_113434.jpg" >}}
{{< figure src="./IMG_20181126_113614.jpg" >}}
{{< figure src="./IMG_20181126_114032.jpg" >}}
{{< figure src="./IMG_20181126_114239.jpg" >}}
{{< figure src="./IMG_20181126_114241.jpg" >}}
{{< figure src="./IMG_20181126_114327.jpg" >}}
{{< figure src="./IMG_20181126_114653.jpg" >}}
{{< figure src="./IMG_20181126_114847.jpg" >}}
{{< figure src="./IMG_20181126_115012.jpg" >}}
{{< figure src="./IMG_20181126_120603.jpg" >}}

# Original Description

In addition to the basic infrastructure as code capabilities provided by AWS CloudFormation, AWS now offers various programmability constructs to power complex provisioning use cases. In this talk, we present several advanced use cases of declarative, imperative, and mixed coding scenarios that cloud infrastructure developers can leverage. Examples include demonstrating how to create custom resources and leveraging transforms, like the AWS Serverless Application Model (AWS SAM), to create both simple and complex macros with AWS CloudFormation.
