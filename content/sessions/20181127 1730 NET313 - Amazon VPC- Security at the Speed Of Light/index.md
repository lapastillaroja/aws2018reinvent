---
title: "NET313 - Amazon VPC: Security at the Speed Of Light"
date: 2018-11-27T17:30:00-08:00
draft: false
tags: ["networking", "level:advanced"]
authors: ["antonio"]
---

# Overview

- very detailed explanation about AWS VPCs works
- explain `security group`s and `flows`

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/5pjG6MDtVYd0UG" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/amazon-vpc-security-at-the-speed-of-light-net313-aws-reinvent-2018" title="Amazon VPC: Security at the Speed Of Light (NET313) - AWS re:Invent 2018" target="_blank">Amazon VPC: Security at the Speed Of Light (NET313) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube

{{< youtube UP7wDBjZ37o >}}

# Original description

With Amazon Virtual Private Cloud (Amazon VPC) you can build your own virtual data center networks in seconds. Every VPC is free, but it comes with enterprise-grade capabilities that would cost millions of dollars in a traditional data center. How is this possible? Come hear how Amazon VPC works under the hood. We uncover how we use Amazon-designed hardware to deliver high-assurance security and ultra-fast performance that makes the speed of light feel slow. Leave with insights and tips for how to optimize your own applications, and even whole organizations, to deliver faster than ever.
