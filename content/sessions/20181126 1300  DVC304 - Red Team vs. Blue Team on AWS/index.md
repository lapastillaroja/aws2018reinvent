---
title: "DVC304 - Red Team vs. Blue Team on AWS"
date: 2018-11-26T13:00:00-08:00
draft: false
tags: ["security", "recommended", "level:advanced"]
authors: ["antonio"]
---

# Overview

- Recommended!
- Good example of how an attacker can get into a system using just aws tools and how to protect it
- Well explained

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/puflmeHw8ZN6dl" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/red-team-vs-blue-team-on-aws-dvc304-aws-reinvent-2018" title="Red Team vs. Blue Team on AWS (DVC304) - AWS re:Invent 2018" target="_blank">Red Team vs. Blue Team on AWS (DVC304) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube

{{< youtube pnwNtlwFYus >}}

# Memo

## Atack (red)

look for rds databases
`aws rds describe-db-instances --filter --query DBInstances[].[DBInstanceIdentifier,MasterUsername,DBSubnetGroup.VPC....`

then get the subnet

after that, we investigate the subnet that has `all traffic allowed`!!

what traffic do DB sec group allow??@

find VPC with access to DB `aws ec2 describe-vpcs --filter "Name..."`

then let's see what's using that Security group

check lambda functions `aws lambda get-function ...`

there's a lambda!!

get the lambda code `aws lambda get-function --function-name xxxx --query Code.location` gives us the url to the code

(see photo)

in the code we find `secrets to access RDS`!

Look for instances that can `exfil` (that)

after that, Sec groups - No outbound restrictions `aws ec2 describe-security-groups --fileter... cidr, Values=0.0.0.0`

We found a WP site! Scan site, exploit vulnerability, upload code to connect to DB, publish to public web site, scrape data

Summary (see photo

## Protect (blue)

Protect credentials

- user training
- passw policies and rotation
- MFA
- require frequent re-auth
- prevent deployment of code with embedded credentials

see photos (initial roles x3)

- IAM stuff
- CloudTrail
- Secret management
  - EC2 param store for secrets (see photo)

### Monitoring

- Amazon GuardDuty
- VPC flow logs
- cloudTrail
- AWS Config

- log shipping
- secure log backups
- automate remediation

### CVE

patch all software & servers regularly -> AWS Systems Manager
CVE scanning - Amazon inspector

### WAF security

see photo
note: ELB can be a good security 

### Network architecture
see photo
note: limit all outbound network traffic!

### original vs better

see photos

## Conclusion

see photo

# Photos

{{< figure src="./IMG_20181126_130745.jpg" >}}
{{< figure src="./IMG_20181126_131450.jpg" >}}
{{< figure src="./IMG_20181126_131826.jpg" >}}
{{< figure src="./IMG_20181126_132221.jpg" >}}
{{< figure src="./IMG_20181126_132225.jpg" >}}
{{< figure src="./IMG_20181126_132257.jpg" >}}
{{< figure src="./IMG_20181126_132403.jpg" >}}
{{< figure src="./IMG_20181126_132424.jpg" >}}
{{< figure src="./IMG_20181126_132611.jpg" >}}
{{< figure src="./IMG_20181126_132616.jpg" >}}
{{< figure src="./IMG_20181126_132857.jpg" >}}
{{< figure src="./IMG_20181126_133720.jpg" >}}
{{< figure src="./IMG_20181126_133806.jpg" >}}
{{< figure src="./IMG_20181126_133841.jpg" >}}
{{< figure src="./IMG_20181126_133923.jpg" >}}
{{< figure src="./IMG_20181126_134143.jpg" >}}

# Original description

Red teamers, penetration testers, and attackers can leverage the same tools used by developers to attack AWS accounts. In this session, two technical security experts demonstrate how an attacker can perform reconnaissance and pivoting on AWS, leverage network, AWS Lambda functions, and implementation weaknesses to steal credentials and data. They then show you how to defend your environment from these threats.

This session is part of re:Invent Developer Community Day, a series led by AWS enthusiasts who share first-hand, technical insights on trending topics.
