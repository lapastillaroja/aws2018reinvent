---
title: "ENT311-R1 - Enterprise DevOps: Patterns of Efficiency"
date: 2018-11-27T15:15:00-08:00
draft: false
tags: ["ci/cd", "devops", "level:advanced", "recommended"]
authors: ["antonio"]
---

# Overview

- General presentation about devops at enterprise level
- *devops is more than CI/CD*
- **Recommended for managers**

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/b2nqRSyawdf32" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/enterprise-devops-patterns-of-efficiency-ent311r1-aws-reinvent-2018" title="Enterprise DevOps: Patterns of Efficiency (ENT311-R1) - AWS re:Invent 2018" target="_blank">Enterprise DevOps: Patterns of Efficiency (ENT311-R1) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube

{{< youtube qyhuMDozWXk >}}

# Memo

## enterprise devops debate

devops is not only ci/cd, there's security, governance, compliance

whats enterprise devops?

- embrace existing and new best practices
- devops legacy apps
- build a culture of inclusion <-- important
- insource value creation

note: devops it's a journey, it takes time

devops

1. enablement and repeatability
enablement happens when you protect the good the behavior and prevent the wrong one!
2. speed - guardrails
start creating templates, put guardrails and then get speed
3. inclusiveness
include the other members of the organization is key (finance, compliance, etc)
4. focus on differentiating factors
5. built-in ~~as-opposed to bolt on~~
give more visibility! scale out!

-> dDevOps done right, drives efficiency (see photo)

## case study: National Australia Bank

...

# Photos

{{< figure src="./IMG_20181127_183428.jpg" >}}
{{< figure src="./IMG_20181127_183922.jpg" >}}
{{< figure src="./IMG_20181127_184315.jpg" >}}
{{< figure src="./IMG_20181127_185820.jpg" >}}
{{< figure src="./IMG_20181127_190018.jpg" >}}
{{< figure src="./IMG_20181127_190417.jpg" >}}

# Original description

DevOps is a powerful movement that can help enterprises speed up their rate of innovation. But many large organizations struggle to implement DevOps at scale due to conflicts (real and perceived) with existing IT processes. Enterprise DevOps is the convergence of the speed and agility from modern development processes with the governance, security, and compliance control from traditional IT operations processes. In this session, learn how to implement enterprise DevOps in your organization through building a culture of inclusion, common sense, and continuous improvement. Also learn how to incorporate the knowledge from subject matter experts across your business into your automated DevOps guardrails to create the positive feedback loop we call "patterns of efficiency." This session contains actionable advice for leaders from IT as well as finance, compliance, and security departments.
