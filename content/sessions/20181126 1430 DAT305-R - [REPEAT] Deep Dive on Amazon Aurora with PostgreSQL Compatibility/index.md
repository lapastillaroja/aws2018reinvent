---
title: "DAT305-R - Deep Dive on Amazon Aurora with PostgreSQL Compatibility"
date: 2018-11-26T14:30:00-08:00
draft: false
tags: ["aurora", "rds", "performance", "recommended"]
authors: ["antonio"]
---

# Overview

- Recommended!
- Super technical!
- It would help you understand the benefits of using Aurora over classic RDS
- Low level details about aurora compared to basic RDS PSQL

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/7pNoRMTR80p6ro" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/deep-dive-on-amazon-aurora-with-postgresql-compatibility-dat305r1-aws-reinvent-2018" title="Deep Dive on Amazon Aurora with PostgreSQL Compatibility (DAT305-R1) - AWS re:Invent 2018" target="_blank">Deep Dive on Amazon Aurora with PostgreSQL Compatibility (DAT305-R1) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube
{{< youtube 3PshvYmTv9M >}}

# Memo

## Introduction

Types

- Aurora PSQL -> Aurora storage (db aware )
- RDS PSQL

both support same stuff

- backup/recovery
- high availability
- **WIP: cross region replication**
- **WIP: outbound logical rep.**

## differences

basic RDS
queued work -> log buffer -> storage

in Aurora
queued work -> storage
^ one improvement (see photo)

### writing less

see photos

note amazon aurora recovers up to 97% faster
note: aurora has no redo: recovers in 3secs!

notes: 30 sec to failover to a RW node including dns changes

### Durability - 4 of 6 quorum

cost of additional sync replicas

2 node 3 node (6 copies) graph: 4 times for 99.9 percentile!
^ aurora gibes 2x lower response times because of the 4 of 6 rule

### replicas and clones

#### replicas
postgreSQL
RW -> ebs -> ebs -> RO can take hours to keep up if postgre is at maximum capacity

compared to that, aurora storage doesn't have the EBS. it uses the same aurora storage and keep up with changes with in memory sync

^ see graphs from video

#### fast clones

see diagrams (fast clones: primary/clone storage)
note: you don't pay for the clone storage. only the data you changed

## coming soon

(a lot of stuff)

- cross region replication

## Caching

- no double buffering

in postgresql you have only 50% mem available for caching. against that aurora has 75% in shared_buffers (survivable cache)

failover (see graphs)
standard psql: 32 sec failover, 340 secs to baseline
enabling CCM (cluster cache management) feature -> same test gives **32 secs to baseline**

## performance

note: see *performance insights* tool

key is **query plan manager**, a nice feature that gives *predictable performance*


## Vacuum

intelligent vacuum prefetch
note: 

## serverless

Aurora serverless: autoscaling db (see photo

## migration

4 methods

- psql pg_dump/ restore
- dms (logical replication)
- read replica

# Photos

{{< figure src="./IMG_20181126_143352.jpg" >}}
{{< figure src="./IMG_20181126_143717.jpg" >}}
{{< figure src="./IMG_20181126_143752.jpg" >}}
{{< figure src="./IMG_20181126_143902.jpg" >}}
{{< figure src="./IMG_20181126_143910.jpg" >}}
{{< figure src="./IMG_20181126_143918.jpg" >}}
{{< figure src="./IMG_20181126_143946.jpg" >}}
{{< figure src="./IMG_20181126_144128.jpg" >}}
{{< figure src="./IMG_20181126_144153.jpg" >}}
{{< figure src="./IMG_20181126_144455.jpg" >}}
{{< figure src="./IMG_20181126_144519.jpg" >}}
{{< figure src="./IMG_20181126_144548.jpg" >}}
{{< figure src="./IMG_20181126_144609.jpg" >}}
{{< figure src="./IMG_20181126_145105.jpg" >}}
{{< figure src="./IMG_20181126_145835.jpg" >}}
{{< figure src="./IMG_20181126_150208.jpg" >}}
{{< figure src="./IMG_20181126_150219.jpg" >}}
{{< figure src="./IMG_20181126_150255.jpg" >}}
{{< figure src="./IMG_20181126_151726.jpg" >}}
{{< figure src="./IMG_20181126_152115.jpg" >}}

# Original description

Amazon Aurora with PostgreSQL compatibility is a relational database service that combines the speed and availability of high-end commercial databases with the simplicity and cost-effectiveness of open-source databases. We review the functionality in order to understand the architectural differences that contribute to improved scalability, availability, and durability. We also dive deep into the capabilities of the service and review the latest available features. Finally, we walk through the techniques that can be used to migrate to Amazon Aurora.

