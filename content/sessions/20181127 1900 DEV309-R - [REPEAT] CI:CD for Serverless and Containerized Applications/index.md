---
title: "DEV309-R - CI/CD for Serverless and Containerized Applications"
date: 2018-11-27T19:00:00-08:00
draft: false
tags: ["serverless", "level:advanced", "ci/cd", "recommended"]
authors: ["antonio"]
---

# Overview

- Why CI/CD is important for success
- Examples on how to do CI/CD with Serverless and Containers
- Introduction to AWS SAM & AWS CDK (new)

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/dfWs3xft6XUmMl" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/cicd-for-serverless-and-containerized-applications-dev309r1-aws-reinvent-2018" title="CI/CD for Serverless and Containerized Applications (DEV309-R1) - AWS re:Invent 2018" target="_blank">CI/CD for Serverless and Containerized Applications (DEV309-R1) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube

{{< youtube 01ewawuL-IY >}}

# Memo

..... arrived late, sorry

new functionality: codedeploy ECS
now supports blue-green deployments

....
## infrastructure as code

phases: build, test, deploy, prod deploy

SAM
- SAM templates: shorhand syntax

# Photos

# Original description

To get the most out of the agility afforded by serverless and containers, it is essential to build CI/CD pipelines that help teams iterate on code and quickly release features. In this talk, we demonstrate how developers can build effective CI/CD release workflows to manage their serverless or containerized deployments on AWS. We cover infrastructure-as-code (IaC) application models, such as AWS Serverless Application Model (AWS SAM) and new imperative IaC tools. We also demonstrate how to set up CI/CD release pipelines with AWS CodePipeline and AWS CodeBuild, and we show you how to automate safer deployments with AWS CodeDeploy.
