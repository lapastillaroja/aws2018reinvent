---
title: "SRV319-R1 - Security & Compliance for Modern Serverless Applications"
date: 2018-11-29T16:00:00-08:00
draft: false
tags: ["serverless", "level:advanced"]
authors: ["antonio"]
---

# Overview

- recommended for all backend engineers!
- hsbc talk: security compliance -> security control was interesting

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/FfatZCBC417wZE" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/security-compliance-for-modern-serverless-applications-srv319r1-aws-reinvent-2018" title="Security &amp; Compliance for Modern Serverless Applications (SRV319-R1) - AWS re:Invent 2018" target="_blank">Security &amp; Compliance for Modern Serverless Applications (SRV319-R1) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube

{{< youtube qtGlE-v3-0U >}}

# Memo


## identity

see photos

## how to secure

tip: 
- don't use long-lived credentials
- periodically rotate API keys for API GW using AWS Secrets Manager

# Original description

Serverless architecture and a microservices approach has changed the way we develop applications. Increased composability doesn't have to mean decreased audibility or security. In this talk, we discuss the security model for applications based on AWS Lambda functions and Amazon API Gateway. Learn about the security and compliance that comes with Lambda right out of the box and with no extra charge or management. We also cover services like AWS Config, AWS Identity and Access Management (IAM), Amazon Cognito, and AWS Secrets Manager available on the platform to help manage application security.