---
title: "SEC403 - Five New Security Automations Using AWS Security Services & Open Source"
date: 2018-11-27T11:30:00-08:00
draft: false
tags: ["security", "level:expert", "recommended"]
authors: ["antonio"]
---

# Overview

- real projects to learn from!
- recommended if you are into security

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/qaGUDJ4TS4RELT" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/five-new-security-automations-using-aws-security-services-open-source-sec403-aws-reinvent-2018" title="Five New Security Automations Using AWS Security Services &amp; Open Source (SEC403) - AWS re:Invent 2018" target="_blank">Five New Security Automations Using AWS Security Services &amp; Open Source (SEC403) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube

{{< youtube M5yQpegaYF8 >}}

# Memo

## introduction

### reactive automation to proactive benefit

see photo

### what's a security service?

two types
sec services
- guardduty, inspector, ..
other services
- lambda, ec2, cloudwatch, etc

note: **lambda is the best service ever** xD

### project health

project at mozilla `how do you asses health`?
(github) mozilla/mig

## project: signalHub

type: centralized message hub for alerts and other notification needs

note: keep simple standardized structure

## project: LambdaCanary

see slides!
note: if lambda checksum fails it's a good idea to get the code and store it in S3 for forensic purposes

## project: guarduty multi-account manager

tip: enable if for all accounts!

and more.....

# Photos

{{< figure src="./IMG_20181127_143403.jpg" >}}
{{< figure src="./IMG_20181127_144536.jpg" >}}
{{< figure src="./IMG_20181127_144906.jpg" >}}

# Original description

In this session, we dive deep into the actual code behind various security automation and remediation functions. We demonstrate each script, describe the use cases, and perform a code review explaining the various challenges and solutions. All use cases are based on customer and C-level feedback and challenges. We look at things like IAM policy scope reduction, alert and ticket integration for security events, forensics and research on AWS resources, secure pipelines, and more. Please join us for a speaker meet-and-greet following this session at the Speaker Lounge (ARIA East, Level 1, Willow Lounge). The meet-and-greet starts 15 minutes after the session and runs for half an hour.