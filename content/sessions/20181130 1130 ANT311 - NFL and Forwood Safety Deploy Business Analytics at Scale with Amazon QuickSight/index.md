---
title: "ANT311 - NFL and Forwood Safety Deploy Business Analytics at Scale with Amazon QuickSight"
date: 2018-11-30T11:30:00-08:00
draft: false
tags: ["quicksight", "glue", "analytics", "level:advanced", "recommended"]
authors: ["antonio"]
---

# Overview

- Recommended
- See how NFL uses Glue, Athena, QuickSight and deliver awesome analytics reports
- QuickSight embedded dashboards!

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/tgQ8g58adaOZ" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/nfl-and-forwood-safety-deploy-business-analytics-at-scale-with-amazon-quicksight-ant311-aws-reinvent-2018" title="NFL and Forwood Safety Deploy Business Analytics at Scale with Amazon QuickSight (ANT311) - AWS re:Invent 2018" target="_blank">NFL and Forwood Safety Deploy Business Analytics at Scale with Amazon QuickSight (ANT311) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube

{{< youtube rJaijdRf7KI >}}

# Photos

{{< figure src="./IMG_20181130_103430.jpg" >}}
{{< figure src="./IMG_20181130_115650.jpg" >}}
{{< figure src="./IMG_20181130_115834.jpg" >}}
{{< figure src="./IMG_20181130_120017.jpg" >}}
{{< figure src="./IMG_20181130_120146.jpg" >}}
{{< figure src="./IMG_20181130_120416.jpg" >}}
{{< figure src="./IMG_20181130_120450.jpg" >}}
{{< figure src="./IMG_20181130_120552.jpg" >}}
{{< figure src="./IMG_20181130_120615.jpg" >}}
{{< figure src="./IMG_20181130_122043.jpg" >}}
{{< figure src="./IMG_20181130_122342.jpg" >}}

# Original description

Enabling interactive data and analytics for thousands of users can be expensive and challenging—from having to forecast usage, provisioning and managing servers, to securing data, governing access, and ensuring auditability. In this session, learn how Amazon QuickSight’s serverless architecture and pay-per-session pricing enabled the National Football League (NFL) and Forwood Safety to roll out interactive dashboards to hundreds and thousands of users. Understand how the NFL utilizes embedded Amazon QuickSight dashboards to provide clubs, broadcasters, and internal users with Next Gen Stats data collected from games. Also, learn about Forwood's journey to enabling dashboards for thousands of Rio Tinto users worldwide, utilizing Amazon QuickSight readers, federated single sign-on, dynamic defaults, email reports, and more.