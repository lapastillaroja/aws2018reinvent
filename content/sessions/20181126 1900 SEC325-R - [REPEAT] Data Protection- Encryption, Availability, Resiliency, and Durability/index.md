---
title: "SEC325-R - Data Protection: Encryption, Availability, Resiliency, and Durability"
date: 2018-11-26T19:00:00-08:00
draft: false
tags: ["security", "encryption", "level:advanced", "recommended"]
authors: ["antonio"]
---

# Overview

- Recommended
- Broad topic about data protection in AWS.
- Well explained

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/1fxylzivuysP4N" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/data-protection-encryption-availability-resiliency-and-durability-sec325r1-aws-reinvent-2018" title="Data Protection: Encryption, Availability, Resiliency, and Durability (SEC325-R1) - AWS re:Invent 2018" target="_blank">Data Protection: Encryption, Availability, Resiliency, and Durability (SEC325-R1) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube 

{{< youtube FH6AXreSQWQ >}}

# Memo

## Agenda

- tenets for data prot.
- AWS IAM - understanding policies
- protecting data in ...

## Whats data prot

Confidentiality
Integrity
Availability

## AWS Secrets Manager

emphasis on: very important to rotate the credentials!

note: rotating cred. you reduce the attack surface

## protection at rest

# Photos

{{< figure src="./IMG_20181126_191323.jpg" >}}
{{< figure src="./IMG_20181126_191649.jpg" >}}
{{< figure src="./IMG_20181126_192116.jpg" >}}
{{< figure src="./IMG_20181126_192556.jpg" >}}
{{< figure src="./IMG_20181126_195028.jpg" >}}
{{< figure src="./IMG_20181126_195510.jpg" >}}
{{< figure src="./IMG_20181126_195849.jpg" >}}

# Original description

Protecting data means ensuring confidentiality, integrity, and availability. In this session, we discuss the full range of data protection capabilities provided by AWS along with a deep dive into AWS Key Management Service (AWS KMS). Learn about data protection strategies for ensuring data integrity and availability using AWS native services that provide durability, recoverability, and resiliency for customer data on AWS. In addition, learn how to define an encryption strategy to protect data cryptographically, including managing KMS permissions, defining key rotation, and best practices for using the AWS Encryption SDK with KMS for custom software development.
