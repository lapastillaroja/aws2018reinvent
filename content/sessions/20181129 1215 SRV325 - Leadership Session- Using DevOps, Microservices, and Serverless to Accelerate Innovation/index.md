---
title: "SRV325 - Leadership Session: Using DevOps, Microservices, and Serverless to Accelerate Innovation"
date: 2018-11-29T12:15:00-08:00
draft: false
tags: ["serverless", "containers", "devops", "level:advanced", "recommended"]
authors: ["antonio"]
---

# Overview

- Good recap about serverless, containers, devops
- Operation model: how Amazon does DevOps (team structure and product ownership)

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/4Y0bbpkScMiE9Y" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/leadership-session-using-devops-microservices-and-serverless-to-accelerate-innovation-srv325-aws-reinvent-2018" title="Leadership Session: Using DevOps, Microservices, and Serverless to Accelerate Innovation (SRV325) - AWS re:Invent 2018" target="_blank">Leadership Session: Using DevOps, Microservices, and Serverless to Accelerate Innovation (SRV325) - AWS re:Invent 2018</a> </strong> from <strong><a href="https://www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube

{{< youtube eXl6Bumksnk >}}

# Memo

## overview

devops: a team as ownership of the project (design, development and operation)

## architectural patterns

### request-reply pattern

when the impact of change is small, release velocity can increase -> monolith over microservice (does one thing)

API GW patterns
see photo

AWS Cloud Map & Aoo Mesh

### event-driven patterns

- messaging (SNS, pub/sub, cloudwatch)
- data stream (or data flow pattern): kinesis & dynamodb

compute: lambda & Fargate (see photo)
serverless: even-driven code execution
Fargate: compute engine for containers (long-running)

NEW lambda layers!

what about monitoring?
app mesh: control plane for microservices (see photos

## changes in delivery

how do I develop & deploy? (see photos)

### release

developers -> services -> delivery pipelines

how amazon does devops (see photos)

tip: use of templates to spread the use of good practices
more on this: 

config rules for code pipeline: useful for keep compliance for pipelines

how do I observe distributed and ephemeral services?
-> AWS x-ray

how do I edit & debug? 

- vscode toolkit!
- AWS CDK! use imperative language to provision infrastructure

Application models simplify building serverless & containerized apps: SAM for serverless

# Photos

TODO

# Original description

In this session, learn how AWS can help you innovate faster with DevOps, microservices, and serverless. Join us for a rare and intimate discussion with AWS senior leaders: David Richardson, VP of Serverless, Ken Exner, director of AWS Developer Tools, and Deepak Singh, director of Compute Services, Containers, and Linux. Hear them share development best practices and discuss key learnings from building modern applications at Amazon.com. Also, learn how developers can leverage containers, AWS Lambda, and developer tools to build and run production applications in the cloud.
