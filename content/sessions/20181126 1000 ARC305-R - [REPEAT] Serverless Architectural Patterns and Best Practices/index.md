---
title: "ARC-305-R Serverless Architectural Patterns and Best Practices"
date: 2018-11-26T10:00:00-08:00
draft: false
tags: ["serverless", "architecture", "best practices", "recommended", "level:advanced"]
authors: ["antonio"]
---

# Overview

- Recommended!
- Good summary of sls patterns and with diagrams easy to understand.
- Well explained

## Slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/e80FCuoQjT4d5P" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/AmazonWebServices/serverless-architectural-patterns-and-best-practices-arc305r2-aws-reinvent-2018" title="Serverless Architectural Patterns and Best Practices (ARC305-R2) - AWS re:Invent 2018" target="_blank">Serverless Architectural Patterns and Best Practices (ARC305-R2) - AWS re:Invent 2018</a> </strong> from <strong><a href="//www.slideshare.net/AmazonWebServices" target="_blank">Amazon Web Services</a></strong> </div>

## Youtube

{{< youtube QdzV04T_kec >}}

# Memo

lambda lifecycle

Lifecycle | start
--- | ---
download + start new container + bootstrap the runtime | **cold start**
start code | warm start
Lambda Power tuning

See! https://github.com/alexcasalboni/aws-lambda-power-tuning

Best practices

- Minimize package size
- Use env variables
- Java: use dagger2, jackson-jr
- **Make sure functions invoked by Amazon SQA don't exceed the Visibility timeout**

Recommendation: use SAM

- CloudFromation for serverless
- resources: functions, APIs, table
- **local testing**

AWS Code services:  AWS CodeStar -> CodeCommit + CodeBuild + ....

Lambda alias: Autopublish alias (see blog)

## First pattern: microservices/API

### API GW

type 1: Edge optimized
API gateway **managed**

CloudFront + Lambda@edge
tip: see AWS blueprints

#### region
disaster recovery scenarios
see 2 photos

#### private
consumers inside vpc

### security

see photo

notes: use resource policies

Cognito
- Identity providers: exchange a token for a IAM policy

### graphQL

Query language for APIs

#### AWS AppSync
see photo

## Pattern 2: Stream processing

types: video streams, data streams, data firehose, data analytics

### data stream

recommendation for near realtime processing:

- source -> data stream -> aws lambda -> other AWS services
- key: buffer size

fan-out pattern

- trades strict message ordering vs high throughput

### real-time analytics

see 2 photos

## pattern 3: data lake

characteristics: see photo

- ingest (kinesis, data FireHorse, iot, )
- **core is S3**
- catalog & search (dynamo, glue, ES)
- analytics & processing (lambda, Athena, Quicksight, glue, RedShift spectrum)
- security & auditing (IAM, kms, cloudtrail, Macie)
- API (API GW)

note: Macie can help to identify PII data in a data lake

see 2 photos (core + foundation)

### search and data catalog

DynamoDB as metadata repository

!see photos!!!! a lot of them

scale lambda: Pywrenh.io 

## pattern 4: machine learning

see photo (machine learning stack)

Example: Amazon Connect. serverless contact center solution

see photo (intelligent CC chatbot)

# All photos

{{< figure src="./IMG_20181126_100955.jpg" >}}
{{< figure src="./IMG_20181126_101906.jpg" >}}
{{< figure src="./IMG_20181126_102004.jpg" >}}
{{< figure src="./IMG_20181126_102059.jpg" >}}
{{< figure src="./IMG_20181126_102304.jpg" >}}
{{< figure src="./IMG_20181126_102540.jpg" >}}
{{< figure src="./IMG_20181126_102808.jpg" >}}
{{< figure src="./IMG_20181126_102927.jpg" >}}
{{< figure src="./IMG_20181126_102959.jpg" >}}
{{< figure src="./IMG_20181126_103030.jpg" >}}
{{< figure src="./IMG_20181126_103304.jpg" >}}
{{< figure src="./IMG_20181126_103304_1.jpg" >}}
{{< figure src="./IMG_20181126_103358.jpg" >}}
{{< figure src="./IMG_20181126_103501.jpg" >}}
{{< figure src="./IMG_20181126_103538.jpg" >}}
{{< figure src="./IMG_20181126_103746.jpg" >}}
{{< figure src="./IMG_20181126_103925.jpg" >}}
{{< figure src="./IMG_20181126_104140.jpg" >}}
{{< figure src="./IMG_20181126_104256.jpg" >}}
{{< figure src="./IMG_20181126_104305.jpg" >}}
{{< figure src="./IMG_20181126_104422.jpg" >}}
{{< figure src="./IMG_20181126_104532.jpg" >}}
{{< figure src="./IMG_20181126_104616.jpg" >}}
{{< figure src="./IMG_20181126_104715.jpg" >}}
{{< figure src="./IMG_20181126_105010.jpg" >}}
{{< figure src="./IMG_20181126_105149.jpg" >}}
{{< figure src="./IMG_20181126_105205.jpg" >}}
{{< figure src="./IMG_20181126_105247.jpg" >}}
{{< figure src="./IMG_20181126_105257.jpg" >}}
{{< figure src="./IMG_20181126_105427.jpg" >}}
{{< figure src="./IMG_20181126_105513.jpg" >}}
{{< figure src="./IMG_20181126_105530.jpg" >}}

# Original Description
As serverless architectures become more popular, customers need a framework of patterns to help them identify how to leverage AWS to deploy their workloads without managing servers or operating systems. This session describes reusable serverless patterns while considering costs. For each pattern, we provide operational and security best practices and discuss potential pitfalls and nuances. We also discuss the considerations for moving an existing server-based workload to a serverless architecture. This session can help you recognize candidates for serverless architectures in your own organizations and understand areas of potential savings and increased agility.
